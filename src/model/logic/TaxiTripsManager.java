package model.logic;

import java.util.ArrayList;

import API.ITaxiTripsManager;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	@Override
	public boolean cargarSistema(String direccionJson) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		return new Lista<TaxiConServicios>();
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C, double latitudReq2C, double longitudReq2C) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}




}
